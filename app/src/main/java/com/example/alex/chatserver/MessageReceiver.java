package com.example.alex.chatserver;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.io.*;
import java.net.*;
import java.util.Scanner;


/**
 * Created by Alex on 15.9.2015.
 */
public class MessageReceiver implements Runnable {

    private ObjectOutputStream output; // For what comes out of your computer
    private ObjectInputStream input; //For what comes to your computer
    private String message = ""; // Message
    private String serverIP; // Server ip
    private Socket s; // Connection between computers
    private Handler handler;
    Scanner reader = new Scanner(System.in);

    public MessageReceiver(Socket s,Handler handler){

        this.s = s;
        this.handler = handler;
    }

    public void run() {

        BufferedReader in = null;

        try

        {
            s.connect(new InetSocketAddress("10.112.205.10", 6789));
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));

            while(true) {

                String inStr;
                inStr = in.readLine();



                Message msg = handler.obtainMessage();
                msg.obj = inStr;
                msg.what = 0;
                handler.sendMessage(msg);
            }

        } catch (Exception e) {
            Log.e("TCP", "C: ERROR", e);


        }finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}